package pl.edu.agh.EksploracjaWeka;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javafx.util.Pair;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.lazy.IBk;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final String FILE_NAME = "c-002.arff";

    private static final double EXP = 5; //TODO do zmiany
	private static final double GAMMA = 0.01; //TODO do zmiany
    private static final int NEAREST_NEIGHBOURS = 5;

    private static final List<Pair<Double, Double>> testExpAndGamma = new ArrayList<>();

    private static final Config[] CONFIGURATIONS = new Config[] {
    		new Config("c-001.arff"),
            new Config("c-002.arff"),
            new Config("c-003.arff"),
            new Config("c-004.arff"),
            new Config("c-005.arff")
    };

    public static void main( String[] args ) {
		try {
			Instances data = readFile(FILE_NAME);
			Classifier cls = new NaiveBayes();
			cls.buildClassifier(data);

			Instance inst = new DenseInstance(3);
			inst.setDataset(data);
			inst.setValue(0, 1.1); // wartośc dla X1
			inst.setValue(1, 2.2); // wartość dla X2

			double y = cls.classifyInstance(inst);
			System.out.println("Y: " + y);

			double[] distrib = cls.distributionForInstance(inst);
			System.out.printf(Locale.US, "%d->%f %d->%f\n", 0, distrib[0], 1, distrib[1]);
			
			System.out.println("Testing classifiers");
			testClassifiers();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void testClassifiers() throws Exception {

	    for (Config c : CONFIGURATIONS) {
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("#################################################");
            System.out.println("Processing file: " + c.getFilename());
            System.out.println("#################################################");
            Instances data = readFile(c.getFilename());
            List<Classifier> simpleClassifiers = new ArrayList<>();
            simpleClassifiers.add(new NaiveBayes());
            simpleClassifiers.add(new J48());
            for (Classifier cls : simpleClassifiers) {
                useClassifier(cls, data, "");
            }
            useClassifier(new IBk(NEAREST_NEIGHBOURS), data, "k = " + NEAREST_NEIGHBOURS);
            for (Double exp : c.getExps()) {
                SMO smoPoly = new SMO();
                smoPoly.setKernel(new PolyKernel(data, 1000, exp, true));
                useClassifier(smoPoly, data, "exp: " + exp);
            }
            for (Double gamma : c.getGammas()) {
                SMO smoRbf = new SMO();
                smoRbf.setKernel(new RBFKernel(data, 0, gamma));
                useClassifier(smoRbf, data, "gamma: " + gamma);
            }
        }

	}
	
	private static void useClassifier(Classifier cls, Instances data, String attributes) throws Exception {
		cls.buildClassifier(data);
	
		Evaluation eval = new Evaluation(data);
		eval.crossValidateModel(cls, data, 10, new Random(1));
		
		System.out.println("#############");
		System.out.println("Using classifier: " + cls.getClass().getSimpleName() + " " + attributes);
		System.out.println("#############");
		
		System.out.println(eval.toSummaryString());
		System.out.println(eval.toMatrixString());
		System.out.printf(Locale.US, "[prec recall fmeasure]:\t%f\t%f\t%f\n", eval.weightedPrecision(), eval.weightedRecall(),
				eval.weightedFMeasure());
	}

	public static void classifyGeneratedInstances() throws Exception {
		Instances data = readFile(FILE_NAME);
		Classifier cls = new NaiveBayes();
		cls.buildClassifier(data);

		List<Attribute> atts = Arrays.asList(new Attribute("X1"), new Attribute("X2"), new Attribute("Y", Arrays.asList("tak","nie")));

		Instances result = new Instances("some-relation", new ArrayList<>(atts),0);
		result.setClassIndex(result.numAttributes()-1);

		for(double x1=-10;x1<=10;x1+=0.1){
			for(double x2=-10;x2<=10;x2+=0.1){
				Instance inst = new DenseInstance(3);
				inst.setValue(0, x1);
				inst.setValue(1, x2);

				inst.setDataset(result);
				double y = cls.classifyInstance(inst);
				inst.setClassValue(y);
				result.add(inst);
			}
		}

		saveToFile("c-001-result.arff", result);
	}

	private static Instances readFile(String filename) throws Exception {
		DataSource ds = new DataSource(filename);
		Instances data = ds.getDataSet();
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes()-1);
		}
		return data;
	}

	private static void saveToFile(String filename, Instances inst) throws IOException {
        ArffSaver saver = new ArffSaver();
        saver.setInstances(inst);
        saver.setFile(new File(filename));
        saver.writeBatch();
    }
}
