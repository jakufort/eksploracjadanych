package pl.edu.agh.EksploracjaWeka;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by Jakub Fortunka on 28.01.2017.
 */

@Data
@RequiredArgsConstructor
public class Config {

    @NonNull private String filename;
    private Double[] exps = new Double[] { 1.0, 2.0, 3.0, 5.0, 8.0, 13.0,  16.0, 25.0 };
    private Double[] gammas = new Double[] { 0.01, 0.1, 1.0, 5.0, 10.0, 15.0, 30.0, 100.0};

}
