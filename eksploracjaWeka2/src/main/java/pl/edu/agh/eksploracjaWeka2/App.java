package pl.edu.agh.eksploracjaWeka2;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import weka.clusterers.AbstractClusterer;
import weka.clusterers.DBScan;
import weka.clusterers.EM;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.gui.visualize.VisualizePanel;

/**
 * Hello world!
 */
public class App {

    private static final boolean RUN_SIMPLEKMEANS = true;
    private static final boolean RUN_DBSCAN = true;
    private static final boolean RUN_EM = true;

    private static final File SIMPLEKMEANS_DIRECTORY = new File("simpleMeans");
    private static final File DBSCAN_IMAGES_DIRECTORY = new File("dbscan");
    private static final File EM_IMAGES_DIRECTORY = new File("em");

    private static final File SIMPLEKMEANS_LOG_FILE = new File("simpleKMeans.log");
    private static final File EM_LOG_FILE = new File("em.log");

    private static final double[] EPSILONS = new double[] {0.001, 0.01, 0.015, 0.02, 0.03, 0.032, 0.033, 0.034, 0.035, 0.038, 0.04, 0.045, 0.05, 0.055, 0.06, 0.07, 0.1};
    private static final double[] BASIC_EPSILONS = new double[] {0.1};
    private static final int[] EM_KS = new int[] {2,3,4,5,6,7,8,9};

   private static final List<Config> CONFIGS = new ArrayList<>(Arrays.asList(
            new Config("cl-001", new int[]{1,2,3,4,5,6,7,8,9,10}, BASIC_EPSILONS, new int[]{1,2,3,4,5,6,7,8,9,10}),
            new Config("cl-002", new int[]{2, 3, 5, 8, 10}, BASIC_EPSILONS, EM_KS),
            new Config("cl-003", new int[]{3}, EPSILONS, EM_KS),
            new Config("cl-004", new int[]{2, 3, 4}, BASIC_EPSILONS, EM_KS),
            new Config("cl-009", new int[]{2, 3}, EPSILONS, new int[]{2,3,4}))
    );

    static {
        int[] ks = new int[] {2,3,4,5,6,7,8,9,10};
        for (int i=5;i<9;i++) {
            CONFIGS.add(new Config("cl-00" + i, ks, EPSILONS, ks));
        }
        CONFIGS.add(new Config("cl-010", ks, EPSILONS, new int[] {4,8}));
    }

    public static void main(String[] args) {
        initializeWorkspace();
        StringBuilder kMeansSb = new StringBuilder();
        StringBuilder emSb = new StringBuilder();
        for (Config c : CONFIGS) {
            try {
                Instances data = readFile(c.getFilename());
                if (RUN_SIMPLEKMEANS) {

                    for (int i = 0; i < c.getKs().length; i++) {
                        int k = c.getKs()[i];
                        SimpleKMeans cls = clusterUsingSimpleKMeans(data, k);
                        runConfig(c, cls, data, "k", String.valueOf(k), SIMPLEKMEANS_DIRECTORY);
                        generateStats(kMeansSb, getKMeansInfo(cls, c.getFilename(), k));
                    }
                }
                if (RUN_DBSCAN) {
                    for (int i = 0; i < c.getEpsilons().length; i++) {
                        double epsilon = c.getEpsilons()[i];
                        DBScan cls = useDBScan(data, epsilon);
                        runConfig(c, cls, data, "e", String.valueOf(epsilon), DBSCAN_IMAGES_DIRECTORY);
                    }
                }
                if (RUN_EM) {
                    for (int i=0;i<c.getEmKs().length;i++) {
                        int k = c.getEmKs()[i];
                        EM cls = useEm(data, k);
                        runConfig(c, cls, data, "k", String.valueOf(k), EM_IMAGES_DIRECTORY);
                        generateStats(emSb, getEMInfo(cls, c.getFilename(), data, k));
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try {
            saveLogs(kMeansSb, SIMPLEKMEANS_LOG_FILE);
            saveLogs(emSb, EM_LOG_FILE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initializeWorkspace() {
        createDirectoryIfNoExists(SIMPLEKMEANS_DIRECTORY);
        createDirectoryIfNoExists(DBSCAN_IMAGES_DIRECTORY);
        createDirectoryIfNoExists(EM_IMAGES_DIRECTORY);
    }

    private static void createDirectoryIfNoExists(File f) {
        if (!f.exists()) {
            f.mkdir();
        }
    }

    private static void runConfig(Config c, AbstractClusterer cls, Instances data, String attributeName, String attributeValue, File directory) throws Exception {
        Instances newData = applyFilterToData(cls, data);
        String screenshotFilename = c.getScrenshotFilename(attributeName, attributeValue);
        visualize(newData, screenshotFilename, screenshotFilename, directory);
    }

    private static void generateStats(StringBuilder sb, String info) {
        sb.append(info);
        System.out.println(info);
    }

    private static void saveLogs(StringBuilder sb, File f) throws IOException {
        if (sb.length() != 0) {
            saveInfoToFile(f, sb.toString());
        }
    }

    private static Instances readFile(String filename) throws Exception {
        DataSource source = new DataSource(filename);
        return source.getDataSet();
    }

    private static SimpleKMeans clusterUsingSimpleKMeans(Instances data, int k) throws Exception {
        SimpleKMeans cls = new SimpleKMeans();
//    	String stringFromClipboard = "-init 0 -max-candidates 100 -periodic-pruning 10000 -min-density 2.0 -t1 -1.25 -t2 -1.0 -N 2 -A \"weka.core.EuclideanDistance -R first-last\" -I 500 -num-slots 1 -S 10";
//    	cls.setOptions(Utils.splitOptions(stringFromClipboard));
        cls.setNumClusters(k);
//    	cls.setSeed(1);
        cls.setPreserveInstancesOrder(true);
        cls.buildClusterer(data);
        return cls;
    }

    private static DBScan useDBScan(Instances data, double epsilon) throws Exception {
        DBScan cls = new DBScan();
        cls.setMinPoints(3);
        cls.setEpsilon(epsilon);
        cls.buildClusterer(data);
        return cls;
    }

    private static EM useEm(Instances data, int k) throws Exception {
        EM cls = new EM();
        cls.setNumClusters(k);
        cls.setSeed(10);
        cls.buildClusterer(data);
        return cls;
    }

    private static Instances applyFilterToData(AbstractClusterer cls, Instances data) throws Exception {
        Add filter = new Add();
        filter.setAttributeIndex("last");
        int num = cls.numberOfClusters();
        StringBuilder sb = new StringBuilder();
        sb.append("cluster0");
//        String labels = "cluster0";
        for (int i = 1; i < num; i++) {
            sb.append(", cluster").append(i);
        }
        filter.setNominalLabels(sb.toString());
        filter.setAttributeName("Cluster");
        filter.setInputFormat(data);
        Instances newData = Filter.useFilter(data, filter);
        int idx = newData.numAttributes() - 1;
        for (int i = 0; i < newData.numInstances(); i++) {
            try {
                int val = cls.clusterInstance(data.get(i));
                if (val != -1) {
                    newData.get(i).setValue(idx, val);
                }
            } catch (Exception e) {

            }
        }
        return newData;
    }

    static void visualize(Instances data, String title, String screenshotFilename, File directory) throws IOException, InterruptedException {
        final JFrame jf = new JFrame(title);
        jf.getContentPane().setLayout(new BorderLayout());
        VisualizePanel vp = new VisualizePanel();
        vp.setShowClassPanel(true);
        jf.getContentPane().add(vp, BorderLayout.CENTER);
        jf.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                jf.dispose();
                System.exit(0);
            }
        });
        jf.pack();
        jf.setSize(600, 450);
        jf.setVisible(true);
        vp.setInstances(data);

        BufferedImage image = new BufferedImage(jf.getWidth(), jf.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = image.createGraphics();
        jf.paintAll(graphics2D);
        graphics2D.dispose();
        ImageIO.write(image, "jpg", new File(directory, screenshotFilename));
        jf.dispose();
    }

    private static String getKMeansInfo(SimpleKMeans cls, String filename, int k) throws IOException {
        return getClustererInfo(filename, "k: " + k, cl -> {
            SimpleKMeans c = (SimpleKMeans) cl;
            Instances centroids = c.getClusterCentroids();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < centroids.numAttributes(); i++) {
                sb.append(centroids.attribute(i)).append(", ");
            }
            for (int i = 0; i < centroids.numInstances(); i++) {
                sb.append(" ").append(centroids.get(i));
            }
            sb.append(" Error: ").append(c.getSquaredError());
            return sb.toString();
        }, cls);
    }

    private static String getEMInfo(EM cls, String filename, final Instances data, int k) throws IOException {
        return getClustererInfo(filename, "k: " + k, cl -> {
            try {
                EM em = (EM) cl;
                double logLikehood = 0.0;
                for(int i=0;i<data.numInstances();i++){
                    double density = em.logDensityForInstance(data.get(i));
                    logLikehood+=density;
                }
                return "LL: " + (logLikehood/data.numInstances());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }, cls);
    }

    private static String getClustererInfo(String filename, String attributeNameValue, Function<AbstractClusterer, String> infoBuilder, AbstractClusterer cls) {
        return "##### Info for: " + filename + " " + attributeNameValue + "\n" +
                infoBuilder.apply(cls) + "\n" +
                "#########" + "\n\n";
    }

    private static void saveInfoToFile(File f, String info) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(f, false));
        bw.write(info);
        bw.close();
    }
}
