package pl.edu.agh.eksploracjaWeka2;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Config {

	@NonNull private String filename;
	@NonNull private int[] ks;
//	@NonNull private String[] screenshotsFiles;
	@NonNull private double[] epsilons;
	@NonNull private int[] emKs;

	public String getFilename() {
		return this.filename + ".arff";
	}

	public String getScrenshotFilename(String attributeName, String attributeValue) {
		return this.filename + attributeName + attributeValue + ".jpg";
	}

}
